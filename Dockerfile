# 设置基础镜像
FROM node:8-slim

ADD . /
RUN npm install --registry=https://registry.npm.taobao.org

# 暴露 8000 端口，便于访问
EXPOSE 8000

# 设置启动时默认运行命令
ENTRYPOINT  npm run dev
